# Installation

Dependencies are managed by Poetry.
https://python-poetry.org/

```bash
$ poetry install
$ poetry shell
```

Running analysis
```
$ python src/main.py --recompute
```
