from xml_handler import XMLHandler
from enum import Enum


class DirType(Enum):
    LEFT = 0
    RIGHT = 1
    SHARPLEFT = 2
    SHARPRIGHT = 3
    SLIGHTLEFT = 4
    SLIGHTRIGHT = 5
    STRAIGHT = 6
    ENTERROUNDABOUT = 7
    EXITROUNDABOUT = 8
    UTURN = 9
    GOAL = 10
    DEPART = 11
    KEEPLEFT = 12
    KEEPRIGHT = 13


class RoutePart:
    def __init__(self, step, dist, dur, label, type_, start_point):
        self.step = step
        self.dist = float(dist)
        self.dur = float(dur)
        self.type_ = DirType(type_)
        self.points = [start_point]
        self.label = label

    def __str__(self):
        return f'''{self.step}:
    dist: {self.dist}
    dur: {self.dur}
    name: {self.label.name}
    ref: {self.label.ref}
    type: {self.type_.name}'''

    def append(self, point):
        self.points.append(point)

    def get_lastpoint(self):
        return self.points[-1]

    def is_lastpoint(self, point):
        return point == self.get_lastpoint()


class Label:
    def __init__(self, name, ref):
        self.name = name
        self.ref = ref

    def __str__(self):
        return f"{self.name}, {self.ref}"


class Point:
    def __init__(self, lat, lon):
        self.lat = lat
        self.lon = lon

    def __str__(self):
        return "{0}, {1}".format(self.lat, self.lon)

    def __eq__(self, other):
        return (self.lat == other.lat and self.lon == other.lon)

    def get_coords(self, format_='lat/lon'):
        if format_ == 'lat/lon':
            return (self.lat, self.lon)
        else:
            # lon/lat
            return (self.lon, self.lat)


class GPXHandler(XMLHandler):

    def __init__(self, infile="input.gpx", outfile="output.gpx"):
        super().__init__(infile, outfile)
        self.parts = None

    def get_points(self):
        rte = self.find('rte')
        return self.findall('rtept', rte)

    def get_coords(self):
        raise NotImplementedError

    def init_parts(self, print_=False):
        parts = list()

        last_step = None
        for point in self.get_points():

            step = self.find('extensions/step', point).text

            if step != last_step:
                dist = self.find('extensions/distance', point).text
                dur = self.find('extensions/duration', point).text
                type_ = int(self.find('extensions/type', point).text)
                start_point = Point(point.attrib['lat'], point.attrib['lon'])

                # part_name can be either:
                # ----------------
                # |  name |  ref  |
                # |-------|-------|
                # |   x   |   x   |
                # |   x   |       |
                # |       |   x   |
                # |       |       |
                # -----------------
                name = self.find('name', point).text
                ref = None
                if not name:
                    label = Label(None, None)
                elif name.find(',') != -1:
                    name, ref = name.split(',')
                    ref = ref.strip()
                    label = Label(name, ref)
                else:
                    # this is a bit hacky but ensures there is no regex matching for ref
                    # and it is easier for query building
                    label = Label(name, name)

                parts.append(RoutePart(step, dist, dur, label, type_, start_point))
            else:
                # append every point in step
                parts[-1].append(Point(point.attrib['lat'], point.attrib['lon']))

            last_step = step

        if print_:
            for part in parts:
                print(part)
        self.start = parts[0].points[0]
        self.end = parts[-1].points[-1]
        self.parts = iter(parts)

    def get_part(self):
        try:
            return next(self.parts)
        except StopIteration:
            return None
