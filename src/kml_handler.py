from sys import argv
from math import isclose
from geopy.distance import geodesic

from xml_handler import XMLHandler


class KMLHandler(XMLHandler):

    def __init__(self, infile="input.kml", outfile="output.kml"):
        super().__init__(infile, outfile)

    def find_closest_point(self, line, point):
        index = 0
        min_index = None
        min_dist = float('inf')
        for coord in line:
            x, y, *rest = coord.split(',')
            # point is      lat, lon, alt
            # our data are  lon, lat
            dist = geodesic((x, y), point).meters
            if isclose(dist, 0.0):
                return index
            if dist < min_dist:
                min_dist = dist
                min_index = index
            index += 1
        return min_index

    def min_dist(self):
        coords = self.get_coords()
        dists = list()
        idx = 1
        for _ in coords:
            try:
                x1, y1, *rest = coords[idx - 1].split(',')
                x2, y2, *rest = coords[idx].split(',')
                dist = int(geodesic((y1, x1), (y2, x2)).meters)
                dists.append(dist)
                # print(y1, x1, y2, x2, ':', dist)
                idx += 1
            except IndexError:
                break
        return dists

    def get_line_elem(self):
        if self.folder:
            search_path = 'Document/Folder/Folder/Placemark/LineString'
        else:
            search_path = 'Document/Placemark/LineString'
        return self.find(search_path)

    def get_coords_elem(self, line):
        return self.find('coordinates', line)

    def get_coords(self, line=None, coords_elem=None):
        if not line:
            line = self.get_line_elem()
        if not coords_elem:
            coords_elem = self.get_coords_elem(line)
        return coords_elem.text.strip().split()

    def _change_style_icon(self):
        hicon_url = self.find("Document/Style[@id='waypoint_h']/IconStyle/Icon/href")
        nicon_url = self.find("Document/Style[@id='waypoint_n']/IconStyle/Icon/href")

        hicon_url.text = "http://maps.google.com/mapfiles/kml/pal2/icon13.png"
        nicon_url.text = "http://maps.google.com/mapfiles/kml/pal2/icon28.png"

    def add_placemarks_points(self, points):
        """
        List[RouteParts] -> _
        """

        # coords are in lon,lat,alt
        # 1st and last points will be highlited with suffix '_h'
        # other points will have style with suffix '_n'
        template = """
            <Placemark>
            <name>{name}</name>
            <styleUrl>#waypoint_{hglt}</styleUrl>
            <Point>
                <coordinates>
                {coords},0
                </coordinates>
            </Point>
            </Placemark>
            """

        folder = self.find('Document/Folder/Folder')

        for idx, point in enumerate(points):
            format_input = {
                'name': 'Rest point',
                'hglt': 'n',
                'coords': ','.join(point.get_coords('lon/lat'))
            }
            if idx == 0:
                format_input['name'] = 'Start'
                format_input['hglt'] = 'h'
            elif idx == len(points) - 1:
                format_input['name'] = 'Finish'
                format_input['hglt'] = 'h'

            placemark = self.create(template.format(**format_input))
            folder.append(placemark)

        self._change_style_icon()

        self.tree.write(self.outfile)

    def cut_kml_line(self, point):
        # where point is tuple (x, y)

        # locate LineString element
        line = self.get_line_elem()
        if not self.is_elem(line):
            print('''Could not locate LineString.
                Check for path kml/Document/Placemark/LineString''')
            return

        # split coordinates by the given point
        elem_coords = self.get_coords_elem(line)
        if not self.is_elem(elem_coords):
            print('''Could not locate coordinates.
                Check for path kml/Document/Placemark/LineString/coordinates''')
            return

        coords = self.get_coords(coords_elem=elem_coords)

        index = self.find_closest_point(coords, point)
        elem_coords.text = ' '.join(coords[:index])

        # Change color in Style "LineStyle"
        search_path = 'Document/Style/LineStyle'
        elem_line_style = self.find(search_path)
        if not self.is_elem(elem_line_style):
            print('''Could not locate LineSyle.
                Check for path kml/Document/style/LineStyle''')
            return

        elem_color = self.find('color', elem_line_style)
        if not self.is_elem(elem_color):
            print('''Could not locate color.
                Check for path kml/Document/style/LineStyle/color''')
            return

        elem_color.text = '411400E6'  # (AARRGGBB)

        # write new KML file
        self.tree.write(self.outfile)


if __name__ == "__main__":
    if len(argv) < 5:
        print('Need infile, "x" and "y" as input')
    else:
        handler = KMLHandler(infile=argv[1], outfile=argv[2])
        handler.cut_kml_line((argv[3], argv[4]))
