from math import isclose
from datetime import timedelta
from progressbar import progressbar
from geopy.distance import geodesic

from breaks_handler import Break


class OutOfParts(Exception):
    def __init__(self, left_t, endpoint=None):
        super().__init__("No more parts in route directions.")

        self.left_t = left_t
        self.endpoint = endpoint


class NotInEndpoint(Exception):
    def __init__(self, endpointA, endpointB):
        super().__init__(f"The routing did not end in GPX endpoint. {endpointA} != {endpointB}")


class Aetr:
    def __init__(self, start=None, dur=0, points=[]):
        if start:
            self.points = [start]
        else:
            self.points = points
        self.dur = dur

    def __str__(self):
        name = f"\n{self.__class__.__name__}\n========================================"
        hours = "Driving for: {} [hh:mm:ss]".format(timedelta(seconds=round(self.dur)))
        points = "Throught: \n\t{}".format('\n\t'.join(str(point) for point in self.points))

        return "\n".join([name, hours, points])

    def append(self, point):
        self.points.append(point)

    def extend_dur(self, dur):
        self.dur += dur

    def get_end(self):
        return self.points[-1]


class Feedback:
    def __init__(self, part=None, rest_point=None, left_t=None):
        self.rest_point = rest_point
        self.part = part
        self.left_t = left_t

    def __str__(self):
        return """Feedback: {part}, {point}, {time}""".format(
            part=self.part,
            point=self.rest_point,
            time=self.left_t)

    def __bool__(self):
        return bool(self.rest_point and self.part)

    def part_completed(self):
        return bool((self.part and self.rest_point) and self.part.is_lastpoint(self.rest_point))


class WaysHandler:
    def __init__(self, db, gpx):
        self.db = db
        self.gpx = gpx

    def serialize_ways(self, file, column='osm_id', count=1):
        while True:
            part = self.gpx.get_part()
            if not part:
                break

            for point in progressbar(part.points):
                res, precision = self.db.get_ways(point, column, count)
                file.write("{osmid}|{label}|{point}|{precision}\n".format(
                    osmid=",".join(str(i) for i in res),
                    label=part.label,
                    point=point,
                    precision=precision))

    def compute_breaks(self, drive):
        aetr = Aetr(self.gpx.start)
        feedback = Feedback()

        # get_stage
        while True:
            # calling API
            stage = drive.next_stage()
            if not stage:
                break

            print(f"{stage}")
            if isinstance(stage, Break):
                aetr.extend_dur(stage.dur)
                # calling API
                continue

            try:
                new_feedback = self.traveled_route(stage, feedback)
                aetr.append(new_feedback.rest_point)
                aetr.extend_dur(new_feedback.left_t)
            except OutOfParts as exp:
                #TODO
                # save stage progress
                print(f"Stopped at {exp.endpoint}")
                print(f"Left time for driving: {exp.left_t}")
                aetr.append(exp.endpoint)
                break

            print(f"Stopped at {new_feedback.rest_point}")
            feedback = new_feedback

        if aetr.get_end() != self.gpx.end:
            raise NotInEndpoint(aetr.get_end(), self.gpx.end)
        return aetr

    def traveled_route(self, stage, feedback):
        '''
        returns Feedback(RoutePart, Point)
        '''
        # duration I can drive
        drive_dur = stage.dur

        # feedback -> part, rest_point, left_t
        if feedback and not feedback.part_completed():
            part = feedback.part
            start_p = feedback.rest_point

            failure, break_point, left_dur = self._fit_in_stage(drive_dur, part, start_p, feedback.left_t)
            if failure:
                return Feedback(part, self.db.get_parking(break_point), left_dur)
            last_part = part
            drive_dur = left_dur

        feedback = Feedback(left_t=stage.dur)

        last_part = None
        left_dur = None
        # get_part
        while True:
            part = self.gpx.get_part()
            start_p = None
            if not part:
                raise OutOfParts(left_dur, last_part.get_lastpoint())

            failure, break_point, left_dur = self._fit_in_stage(drive_dur, part, start_p, feedback.left_t)

            if failure:
                return Feedback(part, self.db.get_parking(break_point), left_dur)
            last_part = part
            drive_dur = left_dur

        return Feedback(last_part, last_part.get_lastpoint(), left_dur)

    def _fit_in_stage(self, goal_t, part, start_p=None, left_t=None):
        '''
        Returns (failure, lastpoint, left_dur)
        '''
        dur = left_t if start_p else part.dur

        goal_t -= dur

        if goal_t < 0 or isclose(goal_t, 0):
            goal_t += dur

            guess_dist = goal_t * (part.dist / part.dur)
            lastpoint = self._cumulative_add(guess_dist, part.points)
            left_dur = dur - goal_t

            return (True, lastpoint, left_dur)

        elif goal_t <= 600:  # we can tolerate 10 minutes
            return (False, part.get_lastpoint(), 0)
        else:
            return (False, part.get_lastpoint(), goal_t)

    @staticmethod
    def _cumulative_add(dist, points):
        ''' Adds distance between points till it reaches dist

        dist -> meters
        points -> List[Point]
        '''

        old_p = None
        for point in points:
            if not old_p:
                old_p = point
                continue
            d = int(geodesic(point.get_coords(), old_p.get_coords()).meters)
            dist -= d
            if isclose(dist, 0.0):
                return point
            if dist < 0:
                # the distance between two points is larger than the needed distance
                # we are not including this last point
                return old_p
            old_p = point

        return old_p

    @staticmethod
    def ways_id_to_file(db, coords, file, progress=False, column='osm_id', count=1):
        # this is deprecated
        # this function works with KML files

        def _corutine(coord, file):
            lon, lat, *rest = coord.split(',')
            res = db.get_ways(lon, lat, column, count)
            file.write("{0}\n".format(",".join(str(i) for i in res)))

        if progress:
            iterator = progressbar(coords)
        else:
            iterator = coords

        for coord in iterator:
            _corutine(coord, file)
