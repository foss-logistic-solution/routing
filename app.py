from typing import List, Union
from molten import App, Route, Include, Middleware, ResponseRendererMiddleware
from molten.openapi import (
    OpenAPIHandler, OpenAPIUIHandler, HTTPSecurityScheme, Metadata
    )

from src.kml_handler import KMLHandler


def cut_kml(infile, outfile, point):
    KMLHandler.cut_kml_line(point)


def hello(name: str, age: int) -> str:
    return f"Hi {name}! I hear you're {age} years old."


def cut(lon: float, lat: float) -> str:
    return f"Request from: /kmlsplit/{lon}/{lat}"


get_docs = OpenAPIUIHandler()

get_schema = OpenAPIHandler(
    metadata=Metadata(
        title="Todo API",
        description="An API for managing todos.",
        version="0.0.0",
    ),
    security_schemes=[
        HTTPSecurityScheme("default", "bearer"),
    ],
    default_security_scheme="default",
)


middleware: List[Middleware] = [
    ResponseRendererMiddleware()
]

routes: List[Union[Route, Include]] = [
    Include("/v1/kmleditor", [
        Route("/split/{lon}/{lat}", cut)
    ]),

    Route("/_docs", get_docs),
    Route("/_schema", get_schema),
]

app = App(
    middleware=middleware,
    routes=routes,
)
