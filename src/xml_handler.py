from defusedxml.ElementTree import parse
from xml.etree import cElementTree as ET


class XMLHandler:

    def __init__(self, infile, outfile):
        self.infile = infile
        self.outfile = outfile

        # parse XML
        self.tree = parse(infile)
        self.root = self.tree.getroot()

        # get namespace
        self.ns = {
            'ns': self.get_namespace()
        }

        self.folder = bool(self.find('Document/Folder'))

    def get_namespace(self):
        return self.root.tag.split("}")[0][1:]

    def find(self, path, root=None):
        ns_path = 'ns:' + path.replace('/', '/ns:')

        if root:
            return root.find(ns_path, self.ns)
        else:
            return self.root.find(ns_path, self.ns)

    def findall(self, path, root=None):
        ns_path = 'ns:' + path.replace('/', '/ns:')

        if root:
            return root.findall(ns_path, self.ns)
        else:
            return self.root.findall(ns_path, self.ns)

    @staticmethod
    def create(xml):
        return ET.fromstring(xml)

    @staticmethod
    def is_elem(elem):
        return ET.iselement(elem)
