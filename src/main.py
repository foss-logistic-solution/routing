"""
Main routing computing logic
"""
import sys
import os
import subprocess
import tempfile
import psycopg2
import requests

from db_handler import DBHandler
from gpx_handler import GPXHandler
from kml_handler import KMLHandler
from ways_handler import WaysHandler, NotInEndpoint
from breaks_handler import Drive


def error(msg):
    print(msg, file=sys.stderr)
    sys.exit(1)


def get_new_route(apikey, aetr):
    # get `rest_points` from aetr
    coordinates = list()
    for point in aetr.points:
        coordinates.append(list(point.get_coords(format_='lon/lat')))

    #           List of points lon/lat
    body = {"coordinates": coordinates, "instructions": "false"}

    headers = {
        'Accept': 'application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8',
        'Authorization': f'{apikey}',
        'Content-Type': 'application/json; charset=utf-8'
    }
    call = requests.post('https://api.openrouteservice.org/v2/directions/driving-hgv/gpx', json=body, headers=headers)

    print(call.status_code, call.reason)

    return call.text


def get_environs():
    try:
        gpx_file_path = os.environ['GPX_FILE']
        drive_file_path = os.environ['DRIVE_FILE']
        apikey = os.environ['OPENROUTES_APIKEY']
    except KeyError as err:
        error(str(err))

    return gpx_file_path, drive_file_path, apikey


def gpx_to_kml(gpx_file, kml_file):
    # gpsbabel -i gpx -f output.gpx -o kml,points=0 -F output.kml
    calls = [['gpsbabel', '-V'],
             ['gpsbabel', '-i', 'gpx', '-f', f'{gpx_file}', '-o', 'kml,points=0', '-F', f'{kml_file}']
             ]

    for cmd in calls:
        print("Running command: '{}'".format(' '.join(cmd)))
        ex_code, stdout, stderr = run_command(cmd)
        if ex_code != 0:
            error(stdout.read())


def run_command(cmd):
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    process.wait()
    return (process.returncode, process.stdout, process.stderr)


def main(outfile='out/output.kml'):
    gpx_file_path, drive_file_path, apikey = get_environs()

    # connect to DB
    try:
        db = DBHandler(dbname="gis", user="osm", host="localhost")
    except psycopg2.OperationalError:
        error(f'Can not connect to database.')

    # load gpx route
    try:
        gpx = GPXHandler(infile=gpx_file_path)
        gpx.init_parts(print_=False)
    except FileNotFoundError:
        error(f'File "{gpx_file_path}" not found.')

    ways = WaysHandler(db, gpx)

    with open(drive_file_path, 'r') as file:
        drive = Drive(file)

    try:
        aetr = ways.compute_breaks(drive)
        print(aetr)
    except NotInEndpoint as exp:
        print(exp)
        return

    new_route = get_new_route(apikey, aetr)

    tmpfile = tempfile.NamedTemporaryFile(mode='w', prefix="gpx_route-", delete=False)
    tmpfile.write(new_route)

    os.makedirs(os.path.dirname(outfile), exist_ok=True)
    gpx_to_kml(gpx_file=tmpfile.name, kml_file=outfile)

    kml_file_path = outfile

    # load kml route
    try:
        kml = KMLHandler(infile=kml_file_path, outfile=kml_file_path)
    except FileNotFoundError:
        error(f'File "{kml_file_path}" not found.')

    kml.add_placemarks_points(aetr.points)


if __name__ == "__main__":
    if len(sys.argv) == 2:
        main(sys.argv[1])
    else:
        main()
