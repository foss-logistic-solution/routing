import psycopg2

from gpx_handler import Point


class DBHandler:

    def __init__(self, dbname, user, host):
        self.conn = psycopg2.connect(dbname="gis", user="osm", host="localhost")
        self.cur = self.conn.cursor()

    def _find_ways(self, point, precision, column, count):
        # returns List[int]
        query = self._build_way_query(precision, column, count)
        self.cur.execute(query, {'lon': point.lon, 'lat': point.lat})
        res = self.cur.fetchall()
        return [w[0] for w in res]

    def _build_way_query(self, precision, column, count):
        template = """
            SELECT {column}
            FROM planet_osm_line as l
            WHERE st_dwithin(
                    l.way,
                    st_transform(st_setsrid(st_point(%(lon)s,%(lat)s),4326), 900913),
                    {delta})
                AND osm_id > 0
                AND (l.route = 'road' OR l.route IS NULL)
            ORDER BY l.way <-> st_transform(st_setsrid(st_point(%(lon)s,%(lat)s),4326), 900913)
            LIMIT {count};
            """

        return template.format(column=column, count=count, delta=precision)

    def get_ways(self, point, column, count):
        # returns (List[int], float)
        precision = 5
        ways = None

        # increase precision to find first match
        while not ways:
            ways = self._find_ways(point, precision, column, count)
            precision += 0.5

        return ways, precision

    def _build_park_query(self, precision):
        return """
        SELECT st_astext(st_transform(st_endpoint(poi.way), 4326))
            FROM (
                SELECT osm_id, amenity, highway, service, way FROM planet_osm_line
                UNION ALL
                SELECT osm_id, amenity, highway, service, way FROM planet_osm_point
                UNION ALL
                SELECT osm_id, amenity, highway, service, way FROM planet_osm_polygon
            ) as poi
            WHERE st_dwithin(
                    poi.way,
                    st_transform(st_setsrid(st_point(%(lon)s,%(lat)s),4326), 900913),
                    {precision})
                AND (poi.amenity = 'parking'
                    OR poi.highway = 'rest_area'
                    OR poi.highway = 'services'
                    OR poi.highway = 'service')
                AND NOT poi.service = 'driveway'
            ORDER BY poi.way <-> st_transform(st_setsrid(st_point(%(lon)s,%(lat)s),4326), 900913)
            LIMIT 1;
        """.format(precision=precision)

    def _find_parking(self, point, precision):
        query = self._build_park_query(precision)
        self.cur.execute(query, {'lon': point.lon, 'lat': point.lat})
        res = self.cur.fetchall()
        return [p[0] for p in res]

    def get_parking(self, point):
        precision = 500
        parking = None

        # increase precision to find first match
        while not parking:
            parking = self._find_parking(point, precision)
            precision += 100

        # POINT(lon lat)
        sidx = parking[0].find('(') + 1
        eidx = parking[0].find(')') - 1
        lon, lat = parking[0][sidx:eidx].split(' ')

        return Point(lat, lon)
