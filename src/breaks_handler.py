from itertools import cycle


class Drive:
    def __init__(self, breaks_file):
        self.stages = list()
        for stage in breaks_file:
            if stage[0] == 'd':
                s = Driving(stage[1:-1])
            else:
                s = Break(stage[1:-1])
            self.stages.append(s)
        self.iter = cycle(self.stages)

    def __str__(self):
        s = []
        for stage in self.stages:
            s.append(str(stage))
        return '\n'.join(s)

    def next_stage(self):
        try:
            return next(self.iter)
        except StopIteration:
            return None


class Stage:
    def __init__(self, dur):
        self.dur = float(dur)
        self.name = None

    def __str__(self):
        return "{name}: {dur:.1f}".format(name=self.name, dur=(self.dur / 3600))


class Break(Stage):
    def __init__(self, dur):
        super().__init__(dur)
        self.name = "Break"


class Driving(Stage):
    def __init__(self, dur):
        super().__init__(dur)
        self.name = "Driving"
